#!/usr/bin/env python
from datetime import datetime, timedelta
import argparse

#Argument Parsing
parser = argparse.ArgumentParser(prog='sleeptime', description='Calculate times to wake up based on bedtimes, and vice versa.')
parser.add_argument('-o', '--offset', metavar='offset', type=int, default=30, help='Change the initial fall-asleep offset. (Default: 30)')
parser.add_argument('-t', '--times', metavar='times', type=int, default=6, help='Change the amount of times generated. (Default: 6)')
parser.add_argument('-24', '--use24format', action='store_true', help='Use 24-hours format (13:30) instead of 12-hours (01:30PM).')
modes = parser.add_mutually_exclusive_group(required=False)
modes.add_argument('-l', '--later', metavar='later', nargs='*', help='Generate wakeup times for a certain bedtime. (Format: 11:30 PM)')
modes.add_argument('-b', '--bedtimes', metavar='bedtimes', nargs='*', help='Generate bedtimes for a certain wakeup time. (Format: 11:30 PM')
args = parser.parse_args()

#Initialize list
list = []

timeformatStr = '%I:%M %p'
timeFormatStr24h = '%H:%M'

#Functions
def wakeupTimesNow(): #Generate times to wake up if you went to bed now
    offsetted = datetime.today() + timedelta(minutes=args.offset)
    for i in range(0, args.times + 1):
        list.append(offsetted + timedelta(hours=1.5*i))
    print('If you are going to bed now, wake up at ')
    for i in range(1, len(list)):
        print(list[i].strftime(timeformatStr) + ' for ' + str(i*1.5) + ' hours of sleep')

def wakeupTimesLater(bedstr): #Generate times to wake up if you went to bed later
    bedtime = parseTime(bedstr)
    offsetted = bedtime + timedelta(minutes=args.offset)
    for i in range(0, args.times + 1):
        list.append(offsetted + timedelta(hours=1.5*i))
    print('If you are going to bed at ' + bedtime.strftime(timeformatStr) + ', wake up at ')
    for i in range(1, len(list)):
        print(list[i].strftime(timeformatStr) + ' for ' + str(i*1.5) + ' hours of sleep')

def bedtimes(bedstr): #Generate bedtimes for a certain wakeup time
    wakeup = parseTime(bedstr)
    offsetted = wakeup - timedelta(minutes=args.offset)
    for i in range(0, args.times + 1):
        list.append(offsetted - timedelta(hours=1.5*i))
    print('If you are waking up at ' + wakeup.strftime(timeformatStr) + ', go to bed at ')
    for i in range(1, len(list)):
        print(list[i].strftime(timeformatStr) + ' for ' + str(i*1.5) + ' hours of sleep')

def parseTime(bedstr): #String to datetime
    try:
        today = datetime.today()
        todstr = str(today.year) + '-' + str(today.month) + '-' + str(today.day)
        return datetime.strptime(todstr + ' ' + bedstr, '%Y-%m-%d' + ' ' + timeformatStr)
    except (ValueError):
        print('Incorrect Formatting, please try again')
        exit()

#Main Function
if args.use24format:
    timeformatStr = timeFormatStr24h
if args.later:
    bedstr = ' '.join(args.later)
    wakeupTimesLater(bedstr)
    exit()
elif args.bedtimes:
    bedstr = ' '.join(args.bedtimes)
    bedtimes(bedstr)
    exit()
else:
    wakeupTimesNow()
    exit()
