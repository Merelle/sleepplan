# sleepplan
Sleep plan is a sleep cycle calculator that calculates times to wake up based on bedtimes, and vice versa.

Sleep cycles are typically 1.5 hours long, and timing your bedtimes can have dramatic effects on your restfulness and alertness throughout the day.

You can run sleepplan by simply running `python sleepplan.py` in a terminal, or installing sleepplan from a repository and simply running `sleepplan`.

## Options
`sleepplan` generates wakeup times for the current time

`sleepplan -t XX` changes the amount of wakeup times generated (Default: 6)

`sleepplan -o XX` changes the initial fall-asleep offset used (Default: 30 minutes)

`sleepplan -24` uses 24-hour format

`sleepplan -h` displays help information

#### Mutually Exclusive Options
`sleepplan -l XX:XX [AM, PM]` generates wakeup times for another time (Format: 3:30 AM)

`sleepplan -b XX:XX [AM, PM]` generates bedtimes for a certain wakeup time (Format: 3:30 AM)

## Information
Contributions are very much encouraged!
Sleepplan is licensed under GPLv3.
